#include "GameObjectFactory.hpp"

#include <cassert>

//Initalise the pointer to the static (note can't do this from within the header)
GameObjectFactory* GameObjectFactory::c_pInstance = 0;

GameObjectFactory::GameObjectFactory()
{

}

GameObjectFactory::~GameObjectFactory()
{

}

/* This sets up the static object - the singleton
this is actually a combination of two patterns, not sure this
is what I'd want if I was teaching this
*/

/*static */
GameObjectFactory& GameObjectFactory::getInstance()
{
    if(c_pInstance == 0)
    {
        c_pInstance = new GameObjectFactory();
    }

    return *c_pInstance;
}

bool GameObjectFactory::Register(const char* pType, tCreator aCreator)
{
    std::string str = std::string(pType);

    /* insert for the standard map returns an ierator to the inserted element and a bool
    denoting whether or not the insertion took place.
    */
    return mCreators.insert(tCreatorMap::value_type(pType, aCreator)).second;
}

/* static */
GameObject* GameObjectFactory::create(const char* pType)
{
    tCreatorMap::iterator i = mCreators.find(std::string(pType));

    if(i != mCreators.end())
    {
        tCreator aCreator = (*i).second;
        return aCreator();
    }

    return 0;
}
