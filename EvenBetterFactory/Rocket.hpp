#ifndef _ROCKET_HPP_
#define _ROCKET_HPP_

#include "GameObject.hpp"

class Rocket : public GameObject
{
    public:
    Rocket();
    ~Rocket();

    /* Overrides */
    void draw();
    void update();
};

#endif

