#ifndef _NPC_HPP_
#define _NPC_HPP_

#include "GameObject.hpp"

class NPC : public GameObject
{
    public:
    NPC();
    ~NPC();

    /* Overrides */
    void draw();
    void update();
};

#endif
